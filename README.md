# README #

This is the repository for a group project to create a Hadoop MapReduce program for characterizing Amazon Movie Reviews through word similarity of different subreddits.

### What is this repository for? ###

* This is a group project for Clemson's CPSC 3620, Distributed and Cluster Computing
* This repository contains 3 java projects for analyzing movie review and subreddit data

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Team Members ###

* Zachary McNellis
* Derek Devera
* Sankar Namboodiri
* Stephen Price