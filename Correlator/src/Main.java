import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;

class ValueComparator implements Comparator<String> {
	 
    Map<String, Integer> map;
 
    public ValueComparator(Map<String, Integer> base) {
        this.map = base;
    }
 
    public int compare(String a, String b) {
        if (map.get(a) >= map.get(b)) {
            return -1;
        } else {
            return 1;
        }
    }
}

public class Main {
	public static void main(String[] args) throws IOException {
		BufferedReader br2 = new BufferedReader(new FileReader("reddit_output.txt"));
		String line;
		HashMap<String, ArrayList<String>> redditMap = new HashMap<String, ArrayList<String>>();
		while ((line = br2.readLine()) != null) {
			ArrayList<String> redditDictionary = new ArrayList<String>();
			String[] entry = line.split("\t");
			for(String word : entry[1].split(" ")) {
			    redditDictionary.add(word.split("=")[0]);
			}
			redditMap.put(entry[0], redditDictionary);
		}		
		br2.close();
		//System.out.println(redditMap);
		
		BufferedReader br = new BufferedReader(new FileReader("movie_output.txt"));
		
		while ((line = br.readLine()) != null) {
			ArrayList<String> movieDictionary = new ArrayList<String>();
			String[] rating = line.split("\t");
			for(String word : rating[1].split(" ")) {
			    movieDictionary.add(word.split("=")[0]);
			}
			
			HashMap<String, Integer> countMap = new HashMap<String, Integer>();
			for (java.util.Map.Entry<String, ArrayList<String>> entry : redditMap.entrySet()) {
				@SuppressWarnings("unchecked")
				ArrayList<String> commonList = (ArrayList<String>) CollectionUtils.retainAll(entry.getValue(), movieDictionary);
				int count = countMap.containsKey(entry.getKey()) ? countMap.get(entry.getKey()) : 0;
        		countMap.put(entry.getKey(), count+commonList.size());
				//System.out.println(commonList);
			}
			TreeMap<String, Integer> sortedMap = sortByValue(countMap);
    		
    		int count=0;
            String printWordList = "";
            for (java.util.Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
            	if (count < 5) {
    	            Integer list = entry.getValue();
    	            printWordList = printWordList.concat(entry.getKey()+"="+list+", ");
    	        	count++;
            	}
            	else {
            		break;
            	}
            }
            System.out.println(rating[0] + " {" + printWordList.substring(0, printWordList.length()-2) + "}");
			//System.out.println(rating[0] + " " + movieDictionary);
			
		}
		br.close();
		
	}
	
	public static TreeMap<String, Integer> sortByValue(HashMap<String, Integer> map) {
		ValueComparator vc =  new ValueComparator(map);
		TreeMap<String,Integer> sortedMap = new TreeMap<String,Integer>(vc);
		sortedMap.putAll(map);
		return sortedMap;
	}
}
