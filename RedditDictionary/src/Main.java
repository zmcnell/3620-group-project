import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

class ValueComparator implements Comparator<String> {
	 
    Map<String, Integer> map;
 
    public ValueComparator(Map<String, Integer> base) {
        this.map = base;
    }
 
    public int compare(String a, String b) {
        if (map.get(a) >= map.get(b)) {
            return -1;
        } else {
            return 1;
        }
    }
}

public class Main {

 static ArrayList<String> stopWords = new ArrayList<String>();
	
 public static class TextArrayWritable extends ArrayWritable {
    public TextArrayWritable() {
        super(Text.class);
    }

    public TextArrayWritable(String[] strings) {
        super(Text.class);
        Text[] texts = new Text[strings.length];
        for (int i = 0; i < strings.length; i++) {
            texts[i] = new Text(strings[i]);
        }
        set(texts);
    }   
 }
	
 public static class Map extends Mapper<LongWritable, Text, Text, ArrayWritable> {
    
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        line = line.replaceAll("&[a-z0-9A-Z]+[;,]", "");
        String cvsSplitBy = ",\\s*(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
        
        // use comma as separator
     	String[] columns = line.split(cvsSplitBy);
     	//System.out.println(Arrays.toString(columns));
      
     	//System.out.println(columns.length);
     	//System.out.println(columns.length+" Subreddit " + columns[2] + " , comment=" + columns[7]);
    	
     	//System.out.println(columns.length);
		if (columns.length == 8) {
		ArrayList<String> removedWords = new ArrayList<String>();
        String comment = columns[7];
        //System.out.println(rating+":"+comment+"\n\n");
        for (String s : comment.split("\\p{Punct}*\\s+[\\s\\p{Punct}]*|(\\p{Punct}$)")) {
        	if (!stopWords.contains(s.toLowerCase())) {
        		removedWords.add(s.toLowerCase());
        	}
        }
        //String[] splited = comment.split("\\s+");
        //System.out.println(Arrays.toString(splited));
        
        String[] splited = removedWords.toArray(new String[removedWords.size()]);
        
        context.write(new Text(columns[2]), new TextArrayWritable(splited));
		}
		else {
			//System.out.println(columns[0]);
		}
    }
 } 
        
 public static class Reduce extends Reducer<Text, TextArrayWritable, Text, Text> {

    public void reduce(Text key, Iterable<TextArrayWritable> values, Context context) 
      throws IOException, InterruptedException {
    	HashMap<String, Integer> map = new HashMap<String, Integer>();
    	
        for (TextArrayWritable val : values) {
        	String[] words = val.toStrings();
        	for (int i=0; i<words.length; i++) {
        		int count = map.containsKey(words[i]) ? map.get(words[i]) : 0;
        		map.put(words[i], count+1);
        	}
        }
        //System.out.println(map);
        TreeMap<String, Integer> sortedMap = sortByValue(map);
		//System.out.println(key+": "+sortedMap);
        
        int count=0;
        String printWordList = "";
        for (java.util.Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
        	if (count < 100) {
	            Integer list = entry.getValue();
	            printWordList = printWordList.concat(entry.getKey()+"="+list+", ");
	        	count++;
        	}
        	else {
        		break;
        	}
        }
        if (printWordList.length() >= 2)
        printWordList = printWordList.substring(0, printWordList.length()-2);
        
        context.write(new Text(key.toString()), new Text(printWordList));
    }
    
    public static TreeMap<String, Integer> sortByValue(HashMap<String, Integer> map) {
		ValueComparator vc =  new ValueComparator(map);
		TreeMap<String,Integer> sortedMap = new TreeMap<String,Integer>(vc);
		sortedMap.putAll(map);
		return sortedMap;
	}
 }
        
 public static void main(String[] args) throws Exception {

 	BufferedReader br = new BufferedReader(new FileReader("stopwords.txt"));
	String stopLine;
	while ((stopLine = br.readLine()) != null) {
		stopWords.add(stopLine);
	}
	br.close();
		
    Configuration conf = new Configuration();
        
    Job job = new Job(conf, "reddit_dictionary");
    job.setJarByClass(Main.class);
    
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(TextArrayWritable.class);
        
    job.setMapperClass(Map.class);
    job.setReducerClass(Reduce.class);
        
    job.setInputFormatClass(TextInputFormat.class);
    job.setOutputFormatClass(TextOutputFormat.class);
        
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
        
    job.waitForCompletion(true);
 }
        
}
